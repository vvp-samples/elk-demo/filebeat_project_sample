# Filebeat Project Sample

It's a project sample to demonstrate work with customs logs by ELK stack (Elasticsearch, Logstash, Kibana).

All you need to run it is docker-compose.

Steps:

1. Change the file:<br />
app/filebeat/filebeat.yml<br />
Set your URL of output.logstash host.

2. Make sure you have running ELK (see elk_sample project).

3. Call the command:<br />
docker-compose up [-d]<br />
from the root directory of the project to start it.

Call:<br />
docker-compose down [-v]<br />
to stop it.