# Pull official base image
FROM python:3.6.9-alpine

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV PYTHONPATH /var/opt/log_generator

# Set work directory
WORKDIR /var/opt/log_generator

# Install dependencies
RUN pip install --upgrade pip && pip install pipenv
COPY ./Pipfile ./Pipfile
COPY ./Pipfile.lock ./Pipfile.lock
RUN pipenv install --skip-lock --system

# Copy project
COPY ./app/ ./

# Run
CMD python main.py