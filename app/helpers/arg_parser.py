import os
from argparse import Namespace, ArgumentParser


def parse() -> Namespace:
    parser = ArgumentParser()

    # Add arguments here

    default_log_config_path = os.path.join('configs', 'logging.ini')
    parser.add_argument('--log-config', type=str, default=default_log_config_path,
                        help='Logging config path.')

    args = parser.parse_args()

    return args
