import logging
import random
from time import sleep


logger = logging.getLogger(__name__)


class User:
    def __init__(self, user_id: int, online: bool):
        self.user_id = user_id
        self.online = online


class LogGenerator:
    def __init__(self, user_count: int):
        self._users = [User(user_id=user_id, online=False) for user_id in range(user_count)]
        self._transaction_id = 0

    def main_loop(self):
        while True:
            user = random.choice(self._users)
            user.online = not user.online
            msg = f'Connected user: {user.user_id}' if user.online else f'Disconnected user: {user.user_id}'
            logger.info(msg)

            delay = random.random()
            sleep(delay)

            user = random.choice(self._users)
            if user.online:
                self._transaction_id += 1
                money_amount = round((random.random() + 1) * random.randrange(10, 100), 2)
                msg = f'Transaction: {self._transaction_id}. User: {user.user_id} paid: {money_amount}'
                logger.info(msg)

                delay = random.random()
                sleep(delay)
