import logging.config
import time
from logging import Logger

from helpers import arg_parser
from services.log_generator_service import LogGenerator


def init_logger(log_config_path: str) -> Logger:
    """Initialize logger"""

    logging.config.fileConfig(log_config_path, disable_existing_loggers=False)
    logger = logging.getLogger(__name__)

    return logger


def main():
    """
    Application entry point.
    Usage: python3 main.py --log-config path_to_log_config_ini
    Default path_to_log_config_ini = configs/logging.ini
    """

    args = arg_parser.parse()
    log_config_path = args.log_config

    logger = init_logger(log_config_path=log_config_path)
    logging.Formatter.converter = time.gmtime

    logger.info('Start application')

    log_generator = LogGenerator(user_count=1000)
    log_generator.main_loop()


if __name__ == '__main__':
    main()
